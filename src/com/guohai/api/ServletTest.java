package com.guohai.api;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2017/12/7.
 */
@WebServlet("/ServletTest.do")
public class ServletTest extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         Map map  = request.getParameterMap();


        Set<String> setKeys =  map.keySet();
        for(String key:setKeys ){
          Object  value = map.get(key);
            System.out.print(key.toString()+"      "+value.toString());
            System.out.print(key.toString()+"      "+value.toString());
        }



        System.out.print(map.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      this.doPost(request,response);
    }


}
